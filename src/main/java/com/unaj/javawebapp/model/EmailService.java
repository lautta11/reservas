package com.unaj.javawebapp.model;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class EmailService {
	
	@Value("${spring.mail.host}")
	private String host;
	@Value("${spring.mail.port}")
	private int port;
	@Value("${spring.mail.username}")
	private String username;
	@Value("${spring.mail.password}")
	private String password;
	
	
	public EmailService(String host, int port, String username, String password) {

		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
	}

	
	public EmailService() {

	}


	public String getHost() {
		return host;
	}


	public void setHost(String host) {
		this.host = host;
	}


	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public void sendEmail(String email, String msj) {
		JavaMailSenderImpl mailSender=new JavaMailSenderImpl();
		mailSender.setHost(this.host);
		mailSender.setPort(this.port);
		mailSender.setUsername(this.username);
		mailSender.setPassword("b5g2j9r2");
		
		Properties prop=new Properties();
		prop.put("mail.transport.protocol","smtp");
		prop.put("mail.smtp.auth",true);
		prop.put("mail.smtp.starttls",true);
		
		
		SimpleMailMessage mailMassage=new SimpleMailMessage();
		mailMassage.setFrom(this.username);
		mailMassage.setTo(email);
		mailMassage.setSubject("Spring Boot App");
		mailMassage.setText(msj);
		
		mailSender.setJavaMailProperties(prop);
		mailSender.send(mailMassage);
		
	}
	
	

}
