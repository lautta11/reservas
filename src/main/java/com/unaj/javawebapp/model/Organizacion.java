package com.unaj.javawebapp.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Organizacion {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private int id;
	
	@Column(name="nombre_organizacion", length = 255)
	@JsonProperty("nombre")
	private String nombre;
	
	@Column
	@JsonProperty("descripcion")
	private String descripcion;
	
	@Column
	@Email
	@JsonProperty("email")
	private String email;
	

	public Organizacion () {
		
	}

	public Organizacion(int id, String nombre, String descripcion, String email) {
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	
	
	
	
	

}
