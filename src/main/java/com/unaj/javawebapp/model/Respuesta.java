package com.unaj.javawebapp.model;

public class Respuesta {
	
	
	private String codigo;
	private String httpStatus;
	private String descripcion;
	
	public Respuesta() {
	}

	public Respuesta(String codigo, String httpStatus, String descripcion) {
		this.codigo = codigo;
		this.httpStatus = httpStatus;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(String httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
	
	
	

}
