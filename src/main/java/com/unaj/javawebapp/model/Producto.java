package com.unaj.javawebapp.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import net.bytebuddy.implementation.bind.annotation.Default;

@Entity
public class Producto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private int id;
	
	@Column
	@JsonProperty("nombre")
	private String nombre;

	@Column
	@JsonProperty("imagen")
	private Byte imagen=0;
	
	@Column
	@JsonProperty("categoria")
	private String categoria;

	@Column
	@JsonProperty("disponible")
	private boolean disponible = true;
		
	// el optional es para declarar la variable como NOTNULL.
	@ManyToOne(optional = false , targetEntity = Organizacion.class)
	@JsonProperty("organizacion")
	private Organizacion organizacion;
	
	
	@OneToMany(targetEntity = ReservaProducto.class)
	@JsonProperty("reservaproducto")
	private List<ReservaProducto>reservaproducto;
	
	

	public Producto() {
		
	}


	public Producto(int id, String nombre, byte imagen, Organizacion organizacion, List<ReservaProducto> reservas, String categoria, boolean disponible) {
		this.id = id;
		this.nombre = nombre;
		this.imagen = imagen;
		this.organizacion = organizacion;
		this.reservaproducto = reservas;
		this.categoria=categoria;
		this.disponible = disponible;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public boolean isDisponible() {
		return disponible;
	}



	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public List<ReservaProducto> getReservaproducto() {
		return reservaproducto;
	}

	public void setReservaproducto(List<ReservaProducto> reservaproducto) {
		this.reservaproducto = reservaproducto;
	}


	public String getCategoira() {
		return categoria;
	}


	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}


	public Organizacion getOrganizacion() {
		return organizacion;
	}


	public void setOrganizacion(Organizacion organizacion) {
		this.organizacion = organizacion;
	}


	public Byte getImagen() {
		return imagen;
	}


	public void setImagen(Byte imagen) {
		this.imagen = imagen;
	}
	
	
	


	
	
}
