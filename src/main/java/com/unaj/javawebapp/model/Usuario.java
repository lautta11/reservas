package com.unaj.javawebapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("id")
	private int id;
	
	@Column(name="nombre", length = 255)
	@JsonProperty("nombre")
	private String nombre;
	
	@Column
	@JsonProperty("apellido")
	private String apellido;
	
	@Column
	@JsonProperty("username")
	private String username;
	
	@Column
	@JsonProperty("password")
	private String password;
	
	@Column
	@JsonProperty("calificacion")
	private int calificacion;
	
	@Column
	@Email
	@JsonProperty("email")
	private String email;
		
	@OneToOne(targetEntity = Rol.class)
	@JsonProperty("rolUsuario")
	private Rol rolUsuario; 
	
	@ManyToOne(targetEntity=Organizacion.class)
	@JsonProperty("organizacion")
	private Organizacion organizacion;
	

	public Usuario() {
	
	}


	public Usuario(int id, String nombre, String apellido, String username, String password, int calificacion,
			String email, Rol rolUsuario, Organizacion organizacion) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.username = username;
		this.password = password;
		this.calificacion = calificacion;
		this.email = email;
		this.rolUsuario = rolUsuario;
		this.organizacion = organizacion;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public int getCalificacion() {
		return calificacion;
	}


	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Organizacion getOrganizacion() {
		return organizacion;
	}


	public void setOrganizacion(Organizacion organizacion) {
		this.organizacion = organizacion;
	}


	public Rol getRolUsuario() {
		return rolUsuario;
	}


	public void setRolUsuario(Rol rolUsuario) {
		this.rolUsuario = rolUsuario;
	}



	
	



	
	


	
	
	
	

	
	
}
