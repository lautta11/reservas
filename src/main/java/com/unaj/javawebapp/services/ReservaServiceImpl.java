package com.unaj.javawebapp.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.unaj.javawebapp.model.EmailService;
import com.unaj.javawebapp.model.EstadoReserva;
import com.unaj.javawebapp.model.Producto;
import com.unaj.javawebapp.model.Reserva;
import com.unaj.javawebapp.model.ReservaProducto;
import com.unaj.javawebapp.model.ReservaServicio;
import com.unaj.javawebapp.model.Servicios;
import com.unaj.javawebapp.model.Usuario;
import com.unaj.javawebapp.repository.EstadoRepository;
import com.unaj.javawebapp.repository.ProductoRepository;
import com.unaj.javawebapp.repository.ReservaProductoRepository;
import com.unaj.javawebapp.repository.ReservaRepository;
import com.unaj.javawebapp.repository.UsuarioRepository;
import com.unaj.javawebapp.repository.ServiciosRepository;

import groovyjarjarpicocli.CommandLine.Model.IGetter;

/**
 * @author Lautaro
 *
 */
@Service("serviceReserva")
public class ReservaServiceImpl {

	private ReservaRepository reservarepo;
	private UsuarioRepository userRepo;
	private ProductoRepository productoRepo;
	private ServiciosRepository servicioRepo;
	
	private EstadoRepository estadorepo;
	
	private ReservaProductoRepository reservaProductorepo;
	
	@Autowired
	public ReservaServiceImpl(ReservaRepository reposerva,UsuarioRepository userrepo,ProductoRepository productoRepo,EstadoRepository estado, ReservaProductoRepository reservaProductorepo,ServiciosRepository servicioRepo) {
		this.reservarepo = reposerva;
		this.userRepo = userrepo;
		this.productoRepo = productoRepo;
		this.estadorepo=estado;
		this.reservaProductorepo=reservaProductorepo;
		this.servicioRepo = servicioRepo;

	}
	
 	public String newReserva(ReservaProducto reservapro, int idUsr) {
 		Date date = new Date();
 		
 		


		Usuario user = userRepo.findById(idUsr).orElse(null);

		//Verifica si el usuario ya tiene reservas, de ser asi agrega la nueva reserpaproducto a la lista
		if(reservarepo.findByUser(idUsr)!=null) {
			Reserva reser=reservarepo.findByUser(idUsr);
			reser.setFechareserva(date);
			reservapro.setReserva(reser);
			
			int idproduct=reservapro.getProducto().getId();
			Producto product=productoRepo.findById(idproduct).orElse(null);
			//Si el producto tiene reservas asignadas , pasamos a ver las fechas que tiene reservadas....
			if(reservaProductorepo.findByProduct(idproduct).isEmpty()==false) {
				/// me trae listas ver mañana !!!!!
				
				List<ReservaProducto>listreservaproducto= reservaProductorepo.findByProduct(idproduct);
				
				for(ReservaProducto r:listreservaproducto) {
					if(r.getFechaReservada().compareTo(reservapro.getFechaReservada())==0 || r.getProducto().equals(product)) {
						
						return "ERROR, YA RESERVADA PARA ESTA FCHA O YA RERSERVASTE ESTE PRODUCTo";
						}
					
					if(r.getFechaDevuelta().compareTo(reservapro.getFechaReservada())>0 && r.getFechaReservada().compareTo(reservapro.getFechaReservada())<=0 ){
						return "Error fechas reseservadas";
					}
					}
					
					
					
				if(product.isDisponible()) {				
							//product.setDisponible(false);												
							//productoRepo.save(product);
							
							
					reservapro.setProducto(product);
							
					List<ReservaProducto>listasdeproducts=reser.getReservaproducto();
							
					listasdeproducts.add(reservapro);
					reser.setReservaproducto(listasdeproducts);
							
							 
					reservarepo.saveAndFlush(reser);
							
					return "Producto reservado con exito";
							
							
							
					}else 
						{
							return "ERROR EL PRODUCTO NO SE ENCUANTRA DISPONIBLE ";
						}
						
							
						}
				
			
			
		
			
		}
		// caso contrario crea una nueva reserva
		else{
		int idproduct=reservapro.getProducto().getId();
		//Si el producto tiene reservas asignadas , pasamos a ver las fechas que tiene reservadas....
		if(reservaProductorepo.findByProduct(idproduct)!=null) {
			List<ReservaProducto> lreserpro=reservaProductorepo.findByProduct(idproduct);
			
			for(ReservaProducto re:lreserpro) {
				while(re.getFechaReservada().compareTo(reservapro.getFechaReservada())==0) {
					
					return "Error, ya se encuentra reservada para esa fecha";
				}
				
			}
				
			
			
				// Guardar datos de la RESERVA
				Reserva reserva = new Reserva();

				reserva.setUser(user);
				reserva.setFechareserva(date);


				// Guardar ReservaProducto

				reservapro.setReserva(reserva);
				
				//settear el producto a NO DISPONBLE
				//int idproduct=reservapro.getProducto().getId();
				Producto product=productoRepo.findById(idproduct).orElse(null);
				//product.setDisponible(false);
				productoRepo.save(product);
				reservapro.setProducto(product);

				List<ReservaProducto> lista = new ArrayList();

				lista.add(reservapro);

				reserva.setReservaproducto(lista);
				
				

				
				// agregar a reserva el reservaproducto

				reservarepo.saveAndFlush(reserva);
				EmailService email=new EmailService();
				email.sendEmail("lautaro.valeiras@hotmail.com", "hola");
				return "Nueva reserva creada";
				
				
			
			
			
		}
		
		//si el producto no tiene reservas asignadas
		else {
			// Guardar datos de la RESERVA
			Reserva reserva = new Reserva();

			reserva.setUser(user);
			reserva.setFechareserva(date);


			// Guardar ReservaProducto

			reservapro.setReserva(reserva);
			
			//settear el producto a NO DISPONBLE
			//int idproduct=reservapro.getProducto().getId();
			Producto product=productoRepo.findById(idproduct).orElse(null);
			//product.setDisponible(false);
			productoRepo.save(product);
			reservapro.setProducto(product);

			List<ReservaProducto> lista = new ArrayList();

			lista.add(reservapro);

			reserva.setReservaproducto(lista);
			
			

			
			// agregar a reserva el reservaproducto

			reservarepo.saveAndFlush(reserva);
			EmailService email=new EmailService();

			email.sendEmail("lautaro.valeiras@hotmail.com", "hola");

			return "Nueva reserva creada";
			
			
			
		}
			
		
		}
		return null;
	}

	
	public Reserva newReserva(ReservaServicio reservaserv, int idUsr) {


		DateFormat diareserva = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		Date date = new Date();

		Usuario user = userRepo.findById(idUsr).orElse(null);

		//Verifica si el usuario ya tiene reservas, de ser asi agrega la nueva reservaServicio a la lista
		if(reservarepo.findByUser(idUsr)!=null) {
			Reserva reser=reservarepo.findByUser(idUsr);
			reser.setFechareserva(date);
			reservaserv.setReserva(reser);
			
			int idservicio=reservaserv.getServicio().getId();
			Servicios servicio=servicioRepo.findById(idservicio).orElse(null);
			servicio.setDisponible(false);
			servicioRepo.save(servicio);
			reservaserv.setServicio(servicio);
			
			List<ReservaServicio> listasdeservicios= reser.getReservaservicio();
			listasdeservicios.add(reservaserv);
			reser.setReservaservicio(listasdeservicios);
			
			
			
			return reservarepo.saveAndFlush(reser);
			
		}// caso contrario crea una nueva reserva
		else{

		// Guardar datos de la RESERVA
		Reserva reserva = new Reserva();

		reserva.setUser(user);
		reserva.setFechareserva(date);


		// Guardar ReservaProducto

		reservaserv.setReserva(reserva);
		
		//settear el producto a NO DISPONBLE
		int idServ=reservaserv.getServicio().getId();
		Servicios serv=servicioRepo.findById(idServ).orElse(null);
		serv.setDisponible(false);
		servicioRepo.save(serv);
		reservaserv.setServicio(serv);

		List<ReservaServicio> lista = new ArrayList();

		lista.add(reservaserv);

		reserva.setReservaservicio(lista);
		
		

		
		// agregar a reserva el reservaproducto

		return reservarepo.saveAndFlush(reserva);
		}
	}
	
	
	public List<Reserva> getReservas() {
		// TODO Auto-generated method stub
		return reservarepo.findAll();
	}

	public EstadoReserva newEstadoReserva(@Valid EstadoReserva estado) {
		
		return estadorepo.save(estado);
	}

	public Reserva deleteReservaProducto(int idRerva,int idreservaPro) {
		Reserva reserva=reservarepo.findById(idRerva).orElse(null);
		ReservaProducto reservapro=reservaProductorepo.findById(idreservaPro).orElse(null);
		String msj="";
		List<ReservaProducto>listadeReservasProductos=reserva.getReservaproducto();
		for(ReservaProducto r:listadeReservasProductos) {
			if(r.getId()==idreservaPro) {
				Reserva r2=r.getReserva();
				
				listadeReservasProductos.remove(r);
				
				Producto p=r.getProducto();
				p.setDisponible(true);
				productoRepo.save(p);
				
				reserva.setReservaproducto(listadeReservasProductos);
				
				reservapro.setReserva(reserva);
				reservaProductorepo.delete(reservapro);
				reservarepo.save(reserva);
				//chequea si se queda sin reservarpdocutos elimina la reserva
				if(r2.getReservaproducto()==null || r2.getReservaproducto().isEmpty()) {
					reservarepo.delete(r2);
					return null;
				}
				return reserva;

			
				
			}	
		}		
		return null;
		
	}

	/*
	 * public String newReserva(Reserva reserva ,int idUsr) {
	 * 
	 * Date date=new Date(); EntityManager manager = null;
	 * 
	 * String msg= "";
	 * 
	 * try {
	 * 
	 * 
	 * //DateFormat diareserva=new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
	 * EstadoReserva estado = reserva.getEstadoreserva();
	 * 
	 * EstadoReserva estado = manager.find(EstadoReserva.class, idEstado); if
	 * (estado == null) { throw new
	 * EntityNotFoundException("No se encuentra el estado de la reserva " +
	 * reserva.getEstadoreserva().getId()); }
	 * 
	 * 
	 * Usuario user=userRepo.findById(idUsr).orElse(null); if (user == null) throw
	 * new Exception("No Existe Usuario, Intente Nuevamente");
	 * 
	 * Reserva reservaAux= new Reserva();
	 * 
	 * List<ReservaProducto> listProductos = reserva.getReservaproducto();
	 * List<ReservaServicio> listServicios = reserva.getReservaservicio();
	 * 
	 * 
	 * List<ReservaProducto> listProductosAux = new ArrayList<ReservaProducto>();
	 * boolean ContieneItemServProd = false;
	 * 
	 * if (listProductos != null && !listProductos.isEmpty()) { ContieneItemServProd
	 * = true; for (ReservaProducto reservaProduct : listProductos) {
	 * ReservaProducto itemReservaProd = new ReservaProducto();
	 * itemReservaProd.setProducto(reservaProduct.getProducto());
	 * itemReservaProd.setReserva(reservaProduct.getReserva());
	 * itemReservaProd.setFechaDevuelta(reservaProduct.getFechaDevuelta());
	 * itemReservaProd.setFechaReservada(reservaProduct.getReserva().getFechareserva
	 * ()); listProductosAux.add(itemReservaProd); }
	 * 
	 * reservaAux.setReservaproducto(listProductosAux);
	 * 
	 * }
	 * 
	 * List<ReservaServicio> listServicioAux = new ArrayList<ReservaServicio>(); if
	 * (listServicios != null && !listServicios.isEmpty()) { ContieneItemServProd =
	 * true; for (ReservaServicio reservaServ : listServicios) { ReservaServicio
	 * itemReservaServ= new ReservaServicio();
	 * itemReservaServ.setServicio(reservaServ.getServicio());
	 * itemReservaServ.setReserva(reservaServ.getReserva());
	 * itemReservaServ.setFechaDevuelta(reservaServ.getFechaDevuelta());
	 * itemReservaServ.setFechaReservada(reservaServ.getReserva().getFechareserva())
	 * ; listServicioAux.add(itemReservaServ); }
	 * 
	 * reservaAux.setReservaservicio(listServicioAux); }
	 * 
	 * if (!ContieneItemServProd) throw new
	 * Exception("No Existen Productos/Servicios, Intente Nuevamente");
	 * 
	 * 
	 * reservaAux.setEstadoreserva(estado); reservaAux.setUser(user);
	 * reservaAux.setFechareserva(date);
	 * 
	 * 
	 * reservarepo.saveAndFlush(reservaAux);
	 * 
	 * 
	 * msg = "Se realizo la reserva Exitosamente";
	 * 
	 * } catch (Exception e) {
	 * 
	 * msg = e.getMessage();
	 * 
	 * }
	 * 
	 * return msg; }
	 */

}
