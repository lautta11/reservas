package com.unaj.javawebapp.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.unaj.javawebapp.model.Reserva;
import com.unaj.javawebapp.model.ReservaProducto;

public interface ReservaProductoRepository extends JpaRepository<ReservaProducto,Serializable> {
	
	@Query(value="select * from reserva_producto where producto_id = (:id)", nativeQuery=true)
	List<ReservaProducto> findByProduct(@Param("id") int id);

}
