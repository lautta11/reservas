package com.unaj.javawebapp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.unaj.javawebapp.model.Organizacion;
import com.unaj.javawebapp.model.Producto;
import com.unaj.javawebapp.services.ProductoServiceImpl;
import com.unaj.javawebapp.services.OrganizacionServiceImpl;


@Controller
public class ProductController {
	
	private ProductoServiceImpl serviceProduct;
	private OrganizacionServiceImpl serviceOrganizacion;
	
	@Autowired
	public ProductController(ProductoServiceImpl serviceproducto, OrganizacionServiceImpl serviceOrganizacion) {
		this.serviceProduct=serviceproducto;
		this.serviceOrganizacion = serviceOrganizacion;
		// TODO Auto-generated constructor stub
	}
	
	
	@GetMapping("/cformProduct")
    public String showFormProduct(Producto prod) {
        return "views/createProduct";
    }
	
	@PostMapping("/cproducto")
	public String addproduct(@RequestBody @Valid Producto pro, BindingResult result) {
		
		if (result.hasErrors()) {
            return "views/createProduct";
        }
         
		Organizacion orga= serviceOrganizacion.findById(pro.getOrganizacion().getId());
//		pro.setOrganizacion(orga);
		serviceProduct.addProduct(pro);	
        
		
        return "Guarado satisfactoriamente";

	}
	
	@GetMapping("/cproducto")
	public String findAllByOrg(){
		
		
        return "views/listProductos";
		
		//return "views/listProductos";  
		
		
	}
	
	

}
