package com.unaj.javawebapp.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.unaj.javawebapp.model.Producto;
import com.unaj.javawebapp.services.ProductoServiceImpl;


@RestController
public class ProductRestController {
	
	private ProductoServiceImpl serviceProduct;
	
	@Autowired
	public ProductRestController(ProductoServiceImpl serviceproducto) {
		this.serviceProduct=serviceproducto;
		// TODO Auto-generated constructor stub
	}
	
	@PostMapping("/producto")
	public Producto addproduct(@RequestBody @Valid Producto pro) {
		
		return serviceProduct.addProduct(pro);
	}
	
	@GetMapping("/producto/{idOrg}")
	public List<Producto> findAllByOrg(Model model, @PathVariable("idOrg") int id){
		
		
		//ModelAndView mav = new ModelAndView("views/listProductos");
		
		List<Producto> list = serviceProduct.listByOrganizacion(id);
		
		//model.addAttribute("products", list);
		
		return serviceProduct.listByOrganizacion(id);  
		
		
	}
	@GetMapping("/producto")
	public List<Producto> findAllByOrg(){
		
		return serviceProduct.listAll();  
	
	

	}
}
