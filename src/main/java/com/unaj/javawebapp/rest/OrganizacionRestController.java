package com.unaj.javawebapp.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unaj.javawebapp.model.Organizacion;
import com.unaj.javawebapp.services.OrganizacionServiceImpl;

@RestController("controllerOrg")
public class OrganizacionRestController {
	
	private final OrganizacionServiceImpl servicio;
	
	@Autowired
	OrganizacionRestController(OrganizacionServiceImpl orgservice) {
		this.servicio=orgservice;
		// TODO Auto-generated constructor stub
	}
	
	@PutMapping("/organizacion")
	public boolean addOrganizacion(@RequestBody @Valid Organizacion org) {
		return servicio.crearOrg(org);	
	}
	
	@PostMapping("/organizacion")
	public boolean updateOrganizacion(@RequestBody @Valid Organizacion org) {
		return servicio.actualizar(org);
		
	}
	
	
	@DeleteMapping("/organizacion/{id}")
	public boolean deletebyId(@PathVariable("id") int id) {
		return servicio.borrar(id);
	}
	
	
	@GetMapping("/organizacion")
	public List<Organizacion> obtener(){
		return servicio.mostrartodo();
		
	}
	

}
