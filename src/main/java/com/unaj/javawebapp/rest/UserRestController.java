package com.unaj.javawebapp.rest;

import com.unaj.javawebapp.model.Organizacion;
import com.unaj.javawebapp.model.Rol;
import com.unaj.javawebapp.model.Usuario;
import com.unaj.javawebapp.services.UserServiceImpl;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController("controllerUser")
public class UserRestController {
	 
    private UserServiceImpl usuarioService;
    
    @Autowired
    public void setProductService(UserServiceImpl usuarioService) {
        this.usuarioService = usuarioService;
    }
    
	@PostMapping("/user")
	public Usuario addUser(@RequestBody @Valid Usuario user) {
		return usuarioService.crearUser(user);	
	}
	
	@PutMapping("/user")
	public String updateUser(@RequestBody @Valid Usuario user) {
		return usuarioService.actualizar(user);
		
	}
	
	
	@DeleteMapping("/user/{id}")
	public String deletebyId(@PathVariable("id") int id) {
		return usuarioService.borrar(id);
	}
	
	
	@GetMapping("/user")
	public List<Usuario> obtener(){
		return usuarioService.mostrartodo();
		
	}
	
	@PostMapping("/rol")
	public Rol newRol(@RequestBody @Valid Rol rol){
		return usuarioService.nuevoRol(rol);
	}
	
	@GetMapping("/rol")
	public List<Rol> listRoles(){
		return usuarioService.listRoles();
	}
	
	

}
